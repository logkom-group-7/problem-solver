import tkinter
from tkinter import messagebox

import matplotlib.pyplot as plt
import networkx as nx
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk


def init_display():
    root = tkinter.Tk()
    root.wm_title("Graph Coloring Solution")
    center_display(root)

    return root


def center_display(root):
    window_width = root.winfo_reqwidth()
    window_height = root.winfo_reqheight()

    position_right = int(root.winfo_screenwidth() / 2 - window_width / 2)
    position_down = int(root.winfo_screenheight() / 3 - window_height / 2)

    root.geometry("+{}+{}".format(position_right, position_down))


def init_canvas(root):
    fig = plt.figure(figsize=(5, 5))
    canvas = FigureCanvasTkAgg(fig, master=root)

    return canvas


def colors_map_to_list(nodes, colors_map):
    colors_list = []
    for node in nodes:
        color = colors_map.get(node)
        if color is not None:
            colors_list.append(color)

    return colors_list


def draw_graph(canvas, nodes, edges, colors_map):
    graph = nx.MultiGraph()
    graph.add_nodes_from(nodes)
    graph.add_edges_from(edges)

    colors_list = colors_map_to_list(graph.nodes(), colors_map)
    nx.draw_networkx(graph, node_color=colors_list, node_size=1000)
    canvas.draw()

    return graph


def draw_toolbar(root, canvas):
    toolbar = NavigationToolbar2Tk(canvas, root, pack_toolbar=False)
    toolbar.update()
    toolbar.pack(side=tkinter.BOTTOM, fill=tkinter.X)

    canvas.get_tk_widget().pack(side=tkinter.TOP, fill=tkinter.BOTH, expand=1)

    return toolbar


def construct_graph_display(nodes, edges, colors_map):
    root = init_display()
    canvas = init_canvas(root)
    draw_graph(canvas, nodes, edges, colors_map)
    draw_toolbar(root, canvas)


def show_graph(nodes, edges, colors_map):
    construct_graph_display(nodes, edges, colors_map)
    tkinter.mainloop()


def show_message(title, message):
    messagebox.showinfo(title, message)
    tkinter.mainloop()
