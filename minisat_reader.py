import os

from constants import MinisatStatus
from constraints import Constraint
from dotenv import load_dotenv
from matplotlib.colors import CSS4_COLORS
from typing import List, Dict


def read_output_file() -> List[str]:
    load_dotenv()
    output_filename = os.getenv("MINISAT_FILE_OUTPUT", "output.txt")

    output_file = open(output_filename, "r")
    text_array = output_file.readlines()
    output_file.close()

    return text_array


def is_satisfiable(text_array: List[str]) -> bool:
    status = text_array[0].strip()

    if status == MinisatStatus.SATISFIABLE.value:
        return True
    elif status == MinisatStatus.UNSATISFIABLE.value:
        return False
    else:
        raise ValueError("Invalid Minisat Status")


def true_interpretations(text_array: List[str]) -> List[str]:
    interpretations = text_array[1].strip().split(" ")

    return list(filter(lambda x: int(x) > 0, interpretations))


def node_color(interpretations: List[str], constraint: Constraint) -> Dict[str, str]:
    node_color_dict = {}
    for literal_id in interpretations:
        literal = constraint.get_literal_by_id(int(literal_id))
        color_code = CSS4_COLORS[literal.color.lower()]
        node_color_dict[literal.node] = color_code

    return node_color_dict
