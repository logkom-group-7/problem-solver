# Tugas Kelompok Logika Komputasional Group 7 - Graph Coloring Problem

## Anggota Kelompok

1. Farhan Azyumardhi Azmi - 1706979234
2. Muhammad Nadhirsyah - 1706039383
3. Razaqa Dhafin Haffiyan - 1706039484

## Instalasi

1. Pastikan Anda dapat menjalankan [MiniSat](http://minisat.se/).
2. Disarankan menggunakan virtual environment untuk meng-install package-package yang diperlukan project ini. Referensi: [Virtual Environment](https://docs.python.org/3/tutorial/venv.html).
3. Install package-package yang diperlukan dengan perintah:

    ```shell
    pip install -r requirements.txt
    ```

    Atau

    ```shell
    pip3 install -r requirements.txt
    ```

    Tergantung dari perintah `pip` yang tersedia pada OS.
4. Buat file `.env` pada root folder. Anda dapat melakukan copy dari file `.env.example` dengan memasukkan perintah:

    ```shell
    cp .env.example .env
    ```

## Menjalankan Program

Jalankan perintah berikut:

```shell
python main.py
```

Atau

```shell
python3 main.py
```

Tergantung dari perintah Python yang tersedia pada OS.
