import os
import subprocess

from constraints import Constraint


def generate_cnf_from_problem(constraint: Constraint) -> str:
    """
    Mengolah definisi problem ke dalam bentuk CNF-miniSAT.
    Keluaran fungsi berupa `str` karena akan menjadi konten dari file input miniSAT.
    """
    at_least_one_color_constraint = constraint.at_least_one_color()
    at_most_one_color_constraint = constraint.at_most_one_color()
    different_colors_constraint = constraint.different_adjacent_colors()
    num_variables = len(constraint.literals)
    num_clauses = (
        len(at_least_one_color_constraint)
        + len(at_most_one_color_constraint)
        + len(different_colors_constraint)
    )
    cnf = (
        constraint.constraint_to_cnf(at_least_one_color_constraint)
        + constraint.constraint_to_cnf(at_most_one_color_constraint)
        + constraint.constraint_to_cnf(different_colors_constraint)
    )
    return f"p cnf {num_variables} {num_clauses}\n" + cnf


def generate_minisat_input(minisat_cnf: str):
    """
    Membentuk file input miniSAT dari string CNF yang diberikan.
    """
    with open("input.txt", "w+") as minisat_input:
        minisat_input.write(minisat_cnf)


def solve_with_minisat(input_filename: str) -> bool:
    """
    Memanggil miniSAT untuk mengecek satisfiability dari `input_file`.
    Mengembalikan `bool` sebagai penanda apakah proses berhasil dijalankan.
    """
    if os.path.exists(input_filename):
        minisat_process = subprocess.run(
            ["minisat", input_filename, "output.txt"],
            stdout=subprocess.PIPE,
            universal_newlines=True,
        )
        print(minisat_process.stdout)
        if minisat_process.returncode in [0, 1, 3]:
            return False
        elif minisat_process.returncode in [10, 20]:
            return True
    return False
