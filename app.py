from tkinter import Tk, Entry, Label, Button, Frame

from matplotlib.colors import CSS4_COLORS

from constraints import Constraint
from graph_visualizer import show_graph, show_message
from minisat_reader import (
    read_output_file,
    is_satisfiable,
    true_interpretations,
    node_color,
)
from utils import generate_cnf_from_problem, generate_minisat_input, solve_with_minisat


class App:
    def __init__(self):
        self.list_city = []
        self.list_neighbor = []
        self.list_color = []
        self.all_entries = []
        self.list_error = []
        self.root = Tk()
        self.color_entry = Entry(self.root)
        self.root.title("Graph Coloring")
        self.color_error_label = Label(self.root, fg="red")

        add_box_button = Button(
            self.root, text="Tambah Kota", fg="Black", command=self.add_city_input
        )
        add_box_button.grid()

        solve_button = Button(self.root, text="Solve", command=self.submit_handle)
        solve_button.grid()

        Label(self.root, text="Warna").grid(row=2, column=0)
        Label(self.root, text="Warna1, Warna2, ...").grid(row=3, column=0)

        self.color_entry.grid(row=4, column=0)

        self.color_error_label.grid(row=5, column=0)

        self.root.mainloop()

    def add_city_input(self):

        frame = Frame(self.root)
        frame.grid()

        Label(frame, text="Kota").grid(row=4, column=0)
        Label(frame, text="Kota1").grid(row=5, column=0)
        city_entry = Entry(frame)
        city_entry.grid(row=6, column=0)

        Label(frame, text="Tetangga").grid(row=4, column=1)
        Label(frame, text="Kota1, Kota2, ...").grid(row=5, column=1)
        neighbour_entry = Entry(frame)
        neighbour_entry.grid(row=6, column=1)

        self.all_entries.append((city_entry, neighbour_entry))

    def submit_handle(self):
        self.cleanup_data()
        self.populate_data()
        if self.input_valid():
            self.color_error_label["text"] = ""
            self.solve_problem()
        else:
            self.show_errors()
        self.cleanup_data()

    def cleanup_data(self):
        self.list_city.clear()
        self.list_color.clear()
        self.list_neighbor.clear()
        self.list_error.clear()

    def populate_data(self):
        for number, (city_entry, neighbour_entry) in enumerate(self.all_entries):
            city_value = city_entry.get()
            if city_value:
                self.list_city.append(city_value)
            neighbour_value = neighbour_entry.get()
            if neighbour_value:
                self.list_neighbor.append(neighbour_value.replace(" ", "").split(","))
            else:
                self.list_neighbor.append([])
        color_value = self.color_entry.get()
        if color_value:
            self.list_color = color_value.replace(" ", "").split(",")
        city_edge = []
        for node_idx in range(len(self.list_city)):
            if self.list_neighbor:
                for neighbour in self.list_neighbor[node_idx]:
                    city_edge.append((self.list_city[node_idx], neighbour))
        self.list_neighbor = city_edge

        print(f"City: {self.list_city}")
        print(f"Neighbour: {self.list_neighbor}")
        print(f"Color: {self.list_color}")

    def input_valid(self):
        return self._color_valid() and self._neighbour_valid() and self._city_valid()

    def _city_valid(self):
        if not self.list_city:
            self.list_error.append("Tambahkan minimal satu kota terlebih dahulu")
            return False
        return True

    def _color_valid(self):
        accepted_colors = list(CSS4_COLORS.keys())
        if self.list_color:
            for color in self.list_color:
                if color.lower() not in accepted_colors:
                    self.list_error.append(
                        "Warna harus dalam bahasa inggris seperti black, white, dll"
                    )
                    return False
        else:
            self.list_error.append("Tambahkan minimal satu warna terlebih dahulu")
            return False
        return True

    def _neighbour_valid(self):
        for neighbour_list in self.list_neighbor:
            for neighbour in neighbour_list:
                if neighbour not in self.list_city:
                    self.list_error.append("kota " + neighbour + " tidak terdaftar")
                    return False
        return True

    def show_errors(self):
        if not self.list_error:
            self.color_error_label["text"] = ""
        else:
            self.color_error_label["text"] = "\n".join(self.list_error)

    def solve_problem(self):
        constraint = Constraint(self.list_city, self.list_neighbor, self.list_color)
        cnf = generate_cnf_from_problem(constraint)
        generate_minisat_input(cnf)
        minisat_called = solve_with_minisat("input.txt")
        if minisat_called:
            self.visualize_output(constraint)
        else:
            show_message("MiniSat Failure", "Gagal Menjalankan MiniSat!")

    def visualize_output(self, constraint):
        inp = read_output_file()

        if is_satisfiable(inp):
            interpretations = true_interpretations(inp)
            colors_map = node_color(interpretations, constraint)
            show_graph(constraint.nodes, constraint.edges, colors_map)
        else:
            show_message(
                "Problem Unsatifisfiable", "Solusi tidak ditemukan! [Unsatisfiable]"
            )
