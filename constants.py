from enum import Enum


class MinisatStatus(Enum):
    SATISFIABLE = "SAT"
    UNSATISFIABLE = "UNSAT"
