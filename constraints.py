from __future__ import annotations

from copy import copy, deepcopy
from typing import List, Tuple


class Literal:
    def __init__(self, node: str, color: str):
        self.node = node
        self.color = color
        self.assigned = True

    def __eq__(self, other: Literal):
        return self.node == other.node and self.color == other.color

    def __neg__(self):
        literal = Literal(self.node, self.color)
        literal.assigned = False
        return literal

    def __str__(self):
        return f"({self.node}, {self.color}, {self.assigned})"

    def __repr__(self):
        return self.__str__()


class NodeEmptyException(Exception):
    def __init__(self):
        super(NodeEmptyException, self).__init__("Given node list is empty")


class Constraint:
    def __init__(
        self, nodes: List[str], edges: List[Tuple[str, str]], colors: List[str]
    ):
        if not nodes:
            raise NodeEmptyException
        self._nodes = nodes
        self._edges = edges
        self._colors = colors
        self._literals = []

        for node in self._nodes:
            for color in self._colors:
                self._literals.append(Literal(node=node, color=color))

    @property
    def nodes(self) -> List[str]:
        return copy(self._nodes)

    @property
    def edges(self) -> List[Tuple[str, str]]:
        return copy(self._edges)

    @property
    def colors(self) -> List[str]:
        return copy(self._colors)

    @property
    def literals(self) -> List[Literal]:
        return deepcopy(self._literals)

    def get_literal_id(self, literal: Literal):
        try:
            return self._literals.index(literal) + 1
        except ValueError:
            return None

    def get_literal_by_id(self, literal_id: int):
        try:
            if literal_id < 0:
                literal_id = -literal_id
                return -(self._literals[literal_id - 1])
            return self._literals[literal_id - 1]
        except IndexError:
            return None

    def at_least_one_color(self) -> List[List[Literal]]:
        constraints = []
        if self._colors:
            for node in self._nodes:
                literals_with_same_node = list(
                    filter(lambda literal, n=node: literal.node == n, self._literals)
                )
                constraints.append(literals_with_same_node)
        return constraints

    def at_most_one_color(self) -> List[List[Literal]]:
        constraints = []
        if self._colors:
            for node in self._nodes:
                for color_idx1 in range(len(self.colors)):
                    for color_idx2 in range(color_idx1 + 1, len(self.colors)):
                        constraints.append(
                            [
                                -Literal(node, self._colors[color_idx1]),
                                -Literal(node, self._colors[color_idx2]),
                            ]
                        )
        return constraints

    def different_adjacent_colors(self) -> List[List[Literal]]:
        constraints = []
        if self._colors:
            for edge in self._edges:
                node1, node2 = edge
                for color in self._colors:
                    constraints.append([-Literal(node1, color), -Literal(node2, color)])
        return constraints

    def constraint_to_cnf(self, constraints: List[List[Literal]]) -> str:
        if constraints:
            cnf = ""
            for clause in constraints:
                for literal in clause:
                    literal_id = self.get_literal_id(literal)
                    if not literal.assigned:
                        literal_id = -literal_id
                    cnf += str(literal_id) + " "
                cnf += "0\n"
            return cnf
        return ""
