from typing import List, Dict
from unittest.case import TestCase

from constraints import Literal, Constraint, NodeEmptyException


class TestLiteral(TestCase):
    def test_literal_representation(self):
        literal = Literal("x", "y")

        self.assertEqual(literal.node, "x")
        self.assertEqual(literal.color, "y")
        self.assertTrue(literal.assigned)

    def test_literal_truth_value(self):
        literal = Literal("x", "y")
        self.assertTrue(literal.assigned)
        self.assertFalse((-literal).assigned)

    def test_literal_equality(self):
        literal1 = Literal("x", "y")
        literal2 = Literal("a", "b")
        self.assertFalse(literal1 == literal2)

        literal3 = Literal("x", "y")
        self.assertTrue(literal1 == literal3)


class TestConstraint(TestCase):
    def test_constraint_representation(self):
        nodes = ["A", "B", "C", "D"]
        edges = [("A", "B"), ("A", "C")]
        colors = ["red", "green", "blue"]
        constraint = Constraint(nodes=nodes, edges=edges, colors=colors)

        self.assertEqual(constraint.nodes, nodes)
        self.assertEqual(constraint.edges, edges)
        self.assertEqual(constraint.colors, colors)

    def test_nodes_cannot_be_empty(self):
        with self.assertRaises(NodeEmptyException):
            nodes = []
            edges = [("A", "B"), ("A", "C")]
            colors = ["red", "green", "blue"]
            Constraint(nodes=nodes, edges=edges, colors=colors)

    def test_constraint_hase_clauses_after_init(self):
        nodes = ["A", "B", "C", "D"]
        edges = [("A", "B"), ("A", "C")]
        colors = ["red", "green", "blue"]
        constraint = Constraint(nodes=nodes, edges=edges, colors=colors)

        self.assertEqual(len(constraint.literals), len(nodes) * len(colors))
        self.assertEqual(
            constraint.literals,
            [
                Literal("A", "red"),
                Literal("A", "green"),
                Literal("A", "blue"),
                Literal("B", "red"),
                Literal("B", "green"),
                Literal("B", "blue"),
                Literal("C", "red"),
                Literal("C", "green"),
                Literal("C", "blue"),
                Literal("D", "red"),
                Literal("D", "green"),
                Literal("D", "blue"),
            ],
        )

    def test_at_least_one_color_constraint(self):
        cases = [
            {
                "name": "Nodes, edges, colors are available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C")],
                "colors": ["red", "green", "blue"],
                "constraints": [
                    [Literal("A", "red"), Literal("A", "green"), Literal("A", "blue")],
                    [Literal("B", "red"), Literal("B", "green"), Literal("B", "blue")],
                    [Literal("C", "red"), Literal("C", "green"), Literal("C", "blue")],
                ],
            },
            {
                "name": "Colors are not available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C")],
                "colors": [],
                "constraints": [],
            },
        ]

        for c in cases:
            constraint = Constraint(
                nodes=c["nodes"], edges=c["edges"], colors=c["colors"]
            )
            constraint_list = constraint.at_least_one_color()
            self.assertConstraintList(c, constraint_list)

    def test_at_least_one_color_constraint_cnf(self):
        cases = [
            {
                "name": "Nodes, edges, colors are available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C")],
                "colors": ["red", "green", "blue"],
                "cnf": ["1 2 3 0", "4 5 6 0", "7 8 9 0"],
            },
            {
                "name": "Colors are not available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C")],
                "colors": [],
                "cnf": [""],
            },
        ]

        for c in cases:
            constraint = Constraint(
                nodes=c["nodes"], edges=c["edges"], colors=c["colors"]
            )
            cnf = constraint.constraint_to_cnf(constraint.at_least_one_color())
            self.assertCNF(c, cnf)

    def test_at_most_one_color_constraint(self):
        cases = [
            {
                "name": "Nodes, edges, colors are available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C")],
                "colors": ["red", "green", "blue", "yellow"],
                "constraints": [
                    [-Literal("A", "red"), -Literal("A", "green")],
                    [-Literal("A", "red"), -Literal("A", "blue")],
                    [-Literal("A", "red"), -Literal("A", "yellow")],
                    [-Literal("A", "green"), -Literal("A", "blue")],
                    [-Literal("A", "green"), -Literal("A", "yellow")],
                    [-Literal("A", "blue"), -Literal("A", "yellow")],
                    [-Literal("B", "red"), -Literal("B", "green")],
                    [-Literal("B", "red"), -Literal("B", "blue")],
                    [-Literal("B", "red"), -Literal("B", "yellow")],
                    [-Literal("B", "green"), -Literal("B", "blue")],
                    [-Literal("B", "green"), -Literal("B", "yellow")],
                    [-Literal("B", "blue"), -Literal("B", "yellow")],
                    [-Literal("C", "red"), -Literal("C", "green")],
                    [-Literal("C", "red"), -Literal("C", "blue")],
                    [-Literal("C", "red"), -Literal("C", "yellow")],
                    [-Literal("C", "green"), -Literal("C", "blue")],
                    [-Literal("C", "green"), -Literal("C", "yellow")],
                    [-Literal("C", "blue"), -Literal("C", "yellow")],
                ],
            },
            {
                "name": "Colors are not available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C")],
                "colors": [],
                "constraints": [],
            },
        ]

        for c in cases:
            constraint = Constraint(
                nodes=c["nodes"], edges=c["edges"], colors=c["colors"]
            )
            constraint_list = constraint.at_most_one_color()
            self.assertConstraintList(c, constraint_list)

    def test_at_most_one_color_constraint_cnf(self):
        cases = [
            {
                "name": "Nodes, edges, colors are available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C")],
                "colors": ["red", "green", "blue", "yellow"],
                "cnf": [
                    "-1 -2 0",
                    "-1 -3 0",
                    "-1 -4 0",
                    "-2 -3 0",
                    "-2 -4 0",
                    "-3 -4 0",
                    "-5 -6 0",
                    "-5 -7 0",
                    "-5 -8 0",
                    "-6 -7 0",
                    "-6 -8 0",
                    "-7 -8 0",
                    "-9 -10 0",
                    "-9 -11 0",
                    "-9 -12 0",
                    "-10 -11 0",
                    "-10 -12 0",
                    "-11 -12 0",
                ],
            },
            {
                "name": "Colors are not available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C")],
                "colors": [],
                "cnf": [""],
            },
        ]

        for c in cases:
            constraint = Constraint(
                nodes=c["nodes"], edges=c["edges"], colors=c["colors"]
            )
            cnf = constraint.constraint_to_cnf(constraint.at_most_one_color())
            self.assertCNF(c, cnf)

    def test_different_adjacent_colors_constraint(self):
        cases = [
            {
                "name": "Nodes, edges, colors are available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C"), ("B", "C")],
                "colors": ["red", "green", "blue", "yellow"],
                "constraints": [
                    [-Literal("A", "red"), -Literal("B", "red")],
                    [-Literal("A", "green"), -Literal("B", "green")],
                    [-Literal("A", "blue"), -Literal("B", "blue")],
                    [-Literal("A", "yellow"), -Literal("B", "yellow")],
                    [-Literal("A", "red"), -Literal("C", "red")],
                    [-Literal("A", "green"), -Literal("C", "green")],
                    [-Literal("A", "blue"), -Literal("C", "blue")],
                    [-Literal("A", "yellow"), -Literal("C", "yellow")],
                    [-Literal("B", "red"), -Literal("C", "red")],
                    [-Literal("B", "green"), -Literal("C", "green")],
                    [-Literal("B", "blue"), -Literal("C", "blue")],
                    [-Literal("B", "yellow"), -Literal("C", "yellow")],
                ],
            },
            {
                "name": "Colors are not available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C")],
                "colors": [],
                "constraints": [],
            },
        ]

        for c in cases:
            constraint = Constraint(
                nodes=c["nodes"], edges=c["edges"], colors=c["colors"]
            )
            constraint_list = constraint.different_adjacent_colors()
            self.assertConstraintList(c, constraint_list)

    def test_different_adjacent_colors_constraint_cnf(self):
        cases = [
            {
                "name": "Nodes, edges, colors are available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C"), ("B", "C")],
                "colors": ["red", "green", "blue", "yellow"],
                "cnf": [
                    "-1 -5 0",
                    "-2 -6 0",
                    "-3 -7 0",
                    "-4 -8 0",
                    "-1 -9 0",
                    "-2 -10 0",
                    "-3 -11 0",
                    "-4 -12 0",
                    "-5 -9 0",
                    "-6 -10 0",
                    "-7 -11 0",
                    "-8 -12 0",
                ],
            },
            {
                "name": "Colors are not available",
                "nodes": ["A", "B", "C"],
                "edges": [("A", "B"), ("A", "C")],
                "colors": [],
                "cnf": [""],
            },
        ]

        for c in cases:
            constraint = Constraint(
                nodes=c["nodes"], edges=c["edges"], colors=c["colors"]
            )
            cnf = constraint.constraint_to_cnf(constraint.different_adjacent_colors())
            self.assertCNF(c, cnf)

    def assertConstraintList(self, case: Dict, constraint_list: List[List[Literal]]):
        self.assertEqual(
            len(constraint_list),
            len(case["constraints"]),
            msg=f"Case {case['name']} --> "
            f"Length of constraint: {len(constraint_list)} != Wanted length: {len(case['constraints'])}",
        )
        for constraint in constraint_list:
            self.assertIn(
                constraint,
                case["constraints"],
                msg=f"Case {case['name']} -->"
                f"{constraint} not in {case['constraints']}",
            )

    def assertCNF(self, case: Dict, cnf: str):
        cnf_list = cnf.strip().split("\n")
        self.assertEqual(
            len(cnf_list),
            len(case["cnf"]),
            msg=f"Case {case['name']} --> "
            f"Length of cnf list: {len(cnf_list)} != Wanted length: {len(case['cnf'])}",
        )
        for conjunction in cnf_list:
            self.assertIn(
                conjunction,
                case["cnf"],
                msg=f"Case {case['name']} --> {conjunction} not {case['cnf']}",
            )
